import './App.css';
import React from "react";

import { useTodoState } from "./useTodoState";

import { TodoItem } from "./components/todoItem";

function TodoApp() {
  const { todos, addTodo, toggleTodo, removeTodo } = useTodoState([]);

  return (
    <div>
      <p>Enter TODOs:</p>
      <AddTodo onSubmit={addTodo} />
      <TodoList todos={todos} onToggle={toggleTodo} onRemove={removeTodo} />
    </div>
  );
}

function AddTodo({ onSubmit }) {
  const [text, setText] = React.useState("");

  const handleSubmit = (e) => {
    e.preventDefault();
    if (!text.trim()) return;
    onSubmit(text);
    setText("");
  };

  return (
    <form onSubmit={handleSubmit}>
      <input value={text} onChange={(e) => setText(e.target.value)} />
      <button type="submit">Add Todo</button>
    </form>
  );
}

function TodoList({ todos, onToggle, onRemove }) {
  return (
    <ul>
      {todos.map((todo, index) => (
        <TodoItem
          key={index}
          todo={todo}
          onToggle={() => onToggle(index)}
          onRemove={() => onRemove(index)}
        />
      ))}
    </ul>
  );
}

export default TodoApp;