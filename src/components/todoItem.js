import React from 'react';
import './todoItem.css';

export function TodoItem({ todo, onToggle, onRemove }) {
    return (
      <li className="todo-item">
        <span
          className="todo-text"
          style={{
            textDecoration: todo.completed ? 'line-through' : 'none',
          }}
          onClick={onToggle}
        >
          {todo.text}
        </span>
        <button className="remove-btn" onClick={onRemove}>
          &#x2715;
        </button>
      </li>
    );
}